package projekti;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Comment<T> extends AbstractPersistable<Long> {
    private String text;
    @ManyToOne
    private User user;
    @ManyToOne
    private File file;
    @ManyToOne
    private Message message;
    @ManyToMany
    private List<User> commentLikes;
}