package projekti;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javassist.bytecode.Descriptor.Iterator;

@Controller
public class DefaultController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private MessageRepository messageRepository;

    @GetMapping("*")
    public String mainpage(Model model) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (me != null) {
            model.addAttribute("friendRequests", me.getFriendRequests());
            model.addAttribute("me", me);
            model.addAttribute("friends", me.getFriends());
        }

        return "index";
    }

    @GetMapping("/users/{profileString}")
    public String profilePage(@PathVariable String profileString, Model model) {
        User user = userRepository.findByProfileString(profileString);
        model.addAttribute("name", user.getDisplayName());
        model.addAttribute("profileString", user.getProfileString());
        String me = SecurityContextHolder.getContext().getAuthentication().getName();
        User myUser = userRepository.findByUsername(me);
        Boolean isAlreadyFriend = false;
        Boolean isFriend = false;
        model.addAttribute("files", user.getAlbum());
        model.addAttribute("user", user);

        for (Friend f : myUser.getFriends()) {
            if (f.getUser().getId() == user.getId()) {
                isAlreadyFriend = true;
                isFriend = true;
            }
        }
        for (Friend f : myUser.getFriendRequests()) {
            if (f.getUser().getId() == user.getId()) {
                isAlreadyFriend = true;
            }
        }
        for (Friend f : myUser.getSentFriendRequests()) {
            System.out.println("user" + user.getId());
            System.out.println("Sent request:" + f.getUser().getId());
            if (f.getUser().getId() == user.getId()) {
                isAlreadyFriend = true;
            }
        }
        if (!me.equals(user.getUsername()) && !isAlreadyFriend) {
            model.addAttribute("cansendfriendrequest", true);
        } else {
            model.addAttribute("cansendfriendrequest", false);
        }

        model.addAttribute("isFriend", isFriend);

        return "profile";
    }

    @PostMapping("/findfriend")
    public String postFindFriends(@RequestParam String filter) {
        User u = userRepository.findByDisplayName(filter);
        return "redirect:/users/" + u.getProfileString();
    }

    @PostMapping("/addfriend")
    @Transactional
    public String addFriend(@RequestParam String profileString) {
        User u = userRepository.findByProfileString(profileString);
        String myUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User myUser = userRepository.findByUsername(myUsername);
        Friend friend = new Friend(myUser);
        List<Friend> requestList = u.getFriendRequests();
        List<Friend> sentRequests = myUser.getSentFriendRequests();
        sentRequests.add(new Friend(u));
        requestList.add(friend);
        u.setFriendRequests(requestList);
        myUser.setSentFriendRequests(sentRequests);
        return "redirect:/users/" + profileString;
    }

    @PostMapping("/acceptfriendrequest")
    @Transactional
    public String acceptFriendRequest(@RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User friendUser = userRepository.findByProfileString(profileString);
        List<Friend> myFriendRequests = me.getFriendRequests();
        for (int i = 0; i < myFriendRequests.size(); i++) {
            Friend f = myFriendRequests.get(i);
            if (f.getUser().getId() == friendUser.getId()) {
                myFriendRequests.remove(i);
                i--;
            }
        }
        me.setFriendRequests(myFriendRequests);

        List<Friend> sentRequests = friendUser.getSentFriendRequests();
        for (int i = 0; i < sentRequests.size(); i++) {
            Friend f = sentRequests.get(i);
            if (f.getUser().getId() == me.getId()) {
                sentRequests.remove(f);
            }
        }
        friendUser.setSentFriendRequests(sentRequests);

        List<Friend> myFriendList = me.getFriends();
        myFriendList.add(new Friend(friendUser));
        me.setFriends(myFriendList);

        List<Friend> friendFriendlist = friendUser.getFriends();
        friendFriendlist.add(new Friend(me));
        friendUser.setFriends(friendFriendlist);

        return "redirect:/";
    }

    @Transactional
    @PostMapping("/rejectfriendrequest")
    public String rejectFriendRequest(@RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User friendUser = userRepository.findByProfileString(profileString);
        List<Friend> myFriendRequests = me.getFriendRequests();
        for (int i = 0; i < myFriendRequests.size(); i++) {
            Friend f = myFriendRequests.get(i);
            if (f.getUser().getId() == friendUser.getId()) {
                myFriendRequests.remove(i);
                i--;
            }
        }
        me.setFriendRequests(myFriendRequests);

        List<Friend> sentRequests = friendUser.getSentFriendRequests();
        for (int i = 0; i < sentRequests.size(); i++) {
            Friend f = sentRequests.get(i);
            if (f.getUser().getId() == me.getId()) {
                sentRequests.remove(f);
            }
        }
        friendUser.setSentFriendRequests(sentRequests);
        return "redirect:/";
    }

    @PostMapping("/file")
    public String sendFile(@RequestParam("file") MultipartFile file, @RequestParam String text) throws Exception {
        File f = new File();
        f.setContent(file.getBytes());
        f.setText(text);
        f.setContentType(file.getContentType());
        f.setContentLength(file.getSize());

        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        f.setUser(me);
        List<File> myAlbum = me.getAlbum();
        myAlbum.add(f);
        if (myAlbum.size() <= 10) {
            me.setAlbum(myAlbum);
            userRepository.save(me);
        }
        return "redirect:/";
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> viewFile(@PathVariable Long id) {
        List<File> filelist = fileRepository.findAll();
        File f = new File();
        for (File file : filelist) {
            if (file.getId() == id) {
                f = file;
            }
        }

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(f.getContentType()));
        headers.setContentLength(f.getContentLength());

        return new ResponseEntity<>(f.getContent(), headers, HttpStatus.CREATED);
    }

    @PostMapping("/setprofilephoto")
    public String setProfilePhoto(@RequestParam Long id) {
        List<File> filelist = fileRepository.findAll();
        File f = new File();
        System.out.println("In method");
        for (File file : filelist) {
            if (file.getId() == id) {
                System.out.println("File found!");
                f = file;
            }
        }

        System.out.println("fileContentType:");
        System.out.println(f.getContentType());

        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        me.setProfilePhoto(f.getContent());
        me.setProfilePhotoType(f.getContentType());
        userRepository.save(me);

        return "redirect:/";
    }

    @PostMapping("/sendmessage")
    @Transactional
    public String sendMessage(@RequestParam String message, @RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User user = userRepository.findByProfileString(profileString);
        Message msg = new Message(message, LocalDateTime.now(), me, user, new ArrayList<>(), new ArrayList<>());
        List<Message> msgs = me.getMessages();
        msgs.add(msg);
        me.setMessages(msgs);

        return "redirect:/users/" + profileString;
    }

    @PostMapping("/comment/file")
    @Transactional
    public String commentFile(@RequestParam String text, @RequestParam String profileString, @RequestParam Long fileId) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Comment> comments = me.getComments();
        File f = fileRepository.findById(fileId).get();
        List<Comment> fileComments = f.getComments();
        Comment c = new Comment(text, me, f, null, new ArrayList<>());
        comments.add(c);
        me.setComments(comments);
        fileComments.add(c);
        f.setComments(fileComments);
        return "redirect:/users/" + profileString;
    }

    @PostMapping("/comment/message")
    @Transactional
    public String commentMessage(@RequestParam String text, @RequestParam Long messageId, @RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        List<Comment> comments = me.getComments();
        Message f = messageRepository.findById(messageId).get();
        List<Comment> messageComments = f.getComments();
        Comment c = new Comment(text, me, null, f, new ArrayList<>());
        comments.add(c);
        me.setComments(comments);
        messageComments.add(c);
        f.setComments(messageComments);
        return "redirect:/users/" + profileString;
    }

    @PostMapping("/like/message")
    @Transactional
    public String likeMessage(@RequestParam Long id, @RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        Message m = messageRepository.findById(id).get();

        List<User> likes = m.getMessageLikes();
        List<Message> myLikes = me.getMessageLikes();
        Boolean isAlready = false;
        for (User u : likes) {
            if (u.getId() == me.getId()) {
                isAlready = true;
            }
        }

        if (!isAlready) {
            likes.add(me);
            m.setMessageLikes(likes);
            myLikes.add(m);
            me.setMessageLikes(myLikes);
        }
        
        return "redirect:/users/" + profileString;
    }

    @PostMapping("/like/file")
    @Transactional
    public String likeFile(@RequestParam Long id, @RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        File m = fileRepository.findById(id).get();

        List<User> likes = m.getFileLikes();
        List<File> myLikes = me.getFileLikes();
        Boolean isAlready = false;
        for (User u : likes) {
            if (u.getId() == me.getId()) {
                isAlready = true;
            }
        }

        if (!isAlready) {
            likes.add(me);
            m.setFileLikes(likes);
            myLikes.add(m);
            me.setFileLikes(myLikes);
        }
        
        return "redirect:/users/" + profileString;
    }

    @PostMapping("/like/comment")
    @Transactional
    public String likeComment(@RequestParam Long id, @RequestParam String profileString) {
        User me = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        Comment m = commentRepository.findById(id).get();

        List<User> likes = m.getCommentLikes();
        List<Comment> myLikes = me.getCommentLikes();
        Boolean isAlready = false;
        for (User u : likes) {
            if (u.getId() == me.getId()) {
                isAlready = true;
            }
        }

        if (!isAlready) {
            likes.add(me);
            m.setCommentLikes(likes);
            myLikes.add(m);
            me.setCommentLikes(myLikes);
        }
        
        return "redirect:/users/" + profileString;
    }

}
