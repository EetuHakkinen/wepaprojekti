package projekti;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.data.jpa.domain.AbstractPersistable;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User extends AbstractPersistable<Long> {

    private String username;
    private String displayName;
    private String passwordHash;
    private String profileString;
    
    @OneToMany(cascade = {CascadeType.ALL})
    private List<Friend> friends;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Friend> friendRequests;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Friend> sentFriendRequests;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<File> album;

    @Lob
    private byte[] profilePhoto;
    private String profilePhotoType;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Message> messages;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Comment> comments;

    @ManyToMany
    private List<Message> messageLikes;

    @ManyToMany
    private List<File> fileLikes;

    @ManyToMany(cascade = {CascadeType.ALL})
    private List<Comment> commentLikes;
}