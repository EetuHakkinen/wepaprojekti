package projekti;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message extends AbstractPersistable<Long> {

    private String message;
    private LocalDateTime time;

    @ManyToOne
    private User creator;

    @ManyToOne
    private User toUser;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Comment> comments;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<User> messageLikes;

}