package projekti;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByProfileString(String profileString);
    User findByDisplayName(String displayName);
}