package projekti;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.data.jpa.domain.AbstractPersistable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class File extends AbstractPersistable<Long> {
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] content;
    private String text;
    private String contentType;
    private Long contentLength;
    @ManyToOne
    private User user;
    @OneToMany(cascade = {CascadeType.ALL})
    private List<Comment> comments;

    @ManyToMany(cascade = {CascadeType.ALL})
    private List<User> fileLikes;
}