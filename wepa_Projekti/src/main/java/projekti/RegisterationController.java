package projekti;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegisterationController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    @Transactional
    public String registerNewUser(@RequestParam String username, @RequestParam String displayName, @RequestParam String profileString, @RequestParam String password) {
        if (userRepository.findByUsername(username) != null || userRepository.findByProfileString(profileString) != null) {
            return "redirect:/register";
        }

        String pwHash = BCrypt.hashpw(password, BCrypt.gensalt());

        User user = new User(username, displayName, pwHash, profileString, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), null, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

        userRepository.save(user);

        return "redirect:/login";
    }
}